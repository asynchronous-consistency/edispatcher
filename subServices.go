package edispatcher

import "context"

// StoredEventData описывает данные события, сохраненного на стороне сервиса,
// который использует библиотеку. Это событие будет в последствии отправлено
// в шину событий.
type StoredEventData struct {
	Id   string `json:"id"`   // ID сохраненного события
	Data string `json:"data"` // Данные сохраненного события
}

// EventTemporaryData описывает базовые данные события, принятого на стороне
// канала. Позволяет передавать не только само событие, но и оригинальный
// источник для внутренних нужд системы.
type EventTemporaryData struct {
	Event  StoredEventData
	Origin interface{}
}

// EventChannelInterface описывает интерфейс канала шины событий
type EventChannelInterface interface {
	// Dispatch выполняет доставку событий в шину
	Dispatch(ctx context.Context, eventsData []string) error

	// Receive выполняет получение событий из шины
	Receive(ctx context.Context, lastEvent string, events chan *EventTemporaryData) error
}

// EventsStoreInterface описывает сервис работы с событиями на стороне
// сервера, использующего библиотеку
type EventsStoreInterface[Tx any] interface {
	// CreateEvents выполняет создание событий для доставки в шину
	CreateEvents(ctx context.Context, data []string, tx Tx) error

	// LoadEvents загружает события, сохраненные для доставки в шину
	LoadEvents(ctx context.Context, tx Tx) ([]*StoredEventData, error)

	// DeleteEvents удаляет события, отправленные в шину
	DeleteEvents(ctx context.Context, eventIds []string, tx Tx) error
}

// EventsReceiveStoreInterface описывает сервис работы с полученными событиями
type EventsReceiveStoreInterface[Tx any] interface {
	// GetLastReceivedEventId возвращает ID последнего принятого события
	GetLastReceivedEventId(ctx context.Context, tx Tx) (string, error)

	// SetLastReceivedEventId устанавливает ID последнего принятого события
	SetLastReceivedEventId(ctx context.Context, eventId string, tx Tx) error
}

// BusEventManagerInterface описывает сервис для работы с событиями на стороне шины
type BusEventManagerInterface interface {
	// CommitEvent подтверждает принятие события на стороне шины
	CommitEvent(ctx context.Context, event *EventTemporaryData) error

	// RejectEvent отклоняет событие на стороне шины
	RejectEvent(ctx context.Context, event *EventTemporaryData) error
}

// TransactionManagerInterface описывает менеджер транзакций
type TransactionManagerInterface[Tx any] interface {
	// Begin - Начало транзакции
	Begin(ctx context.Context) (Tx, error)

	// Rollback - Откат изменений
	Rollback(ctx context.Context, tx Tx)

	// Commit - Коммит изменений
	Commit(ctx context.Context, tx Tx)
}
