package main

import (
	"context"
)

// Подписчик на события
type receiver[Tx any] struct {
	received int
}

// Receive - Получение и обработка события
func (r *receiver[Tx]) Receive(ctx context.Context, eventData string, tx Tx) error {
	r.received += 1

	//if r.received > 2500 {
	//    return errors.New(`test`)
	//}

	return nil
}
