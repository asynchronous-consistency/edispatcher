package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
)

// Сервис для работы с событиями на стороне шины
type busManager struct{}

// CommitEvent - Подтверждение принятия события на стороне шины
func (b busManager) CommitEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}

// RejectEvent - Отклонение принятия события на стороне шины
func (b busManager) RejectEvent(context.Context, *edispatcher.EventTemporaryData) error {
	return nil
}
