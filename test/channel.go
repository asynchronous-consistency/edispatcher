package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"github.com/sirupsen/logrus"
	"strconv"
)

// Канал для тестирования
type channel struct {
	bus  chan string
	iter int
}

// Dispatch - Доставка событий в шину
func (c *channel) Dispatch(ctx context.Context, eventsData []string) error {
	c.iter++

	defer func() {
		if e := recover(); nil != e {
			logrus.Debug(e)
		}
	}()

	for _, event := range eventsData {
		select {
		case <-ctx.Done():
			return nil
		default:
		}

		//if c.iter > 30 {
		//	return errors.New(`test`)
		//}

		c.bus <- event
	}

	return nil
}

// Receive - Получение событий из шины в очередь
func (c *channel) Receive(ctx context.Context, lastEvent string, events chan *edispatcher.EventTemporaryData) error {
	defer func() {
		close(c.bus)
	}()

	id := int64(0)
	for {
		select {
		case <-ctx.Done():
			return nil
		case data := <-c.bus:
			id++
			events <- &edispatcher.EventTemporaryData{
				Event: edispatcher.StoredEventData{
					Id:   strconv.FormatInt(id, 10),
					Data: data,
				},
				Origin: nil,
			}
		}
	}
}
