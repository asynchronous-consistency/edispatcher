package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"context"
	"fmt"
	"log"
)

// Тестовый store
type store[Tx any] struct {
	iter int
}

// CreateEvents - Доставка события
func (s *store[Tx]) CreateEvents(_ context.Context, data []string, _ Tx) error {
	log.Println(data)

	return nil
}

// LoadEvents - Прием и обработка событий
func (s *store[Tx]) LoadEvents(context.Context, Tx) ([]*edispatcher.StoredEventData, error) {
	const size = 20
	s.iter++

	//if s.iter > 100 {
	//	return nil, errors.New(`test`)
	//}

	result := make([]*edispatcher.StoredEventData, size)
	for i := 0; i < size; i++ {
		result[i] = &edispatcher.StoredEventData{
			Id:   fmt.Sprintf(`%v`, i+1),
			Data: "Test",
		}
	}

	return result, nil
}

// DeleteEvents - Отправка событий в шину
func (s *store[Tx]) DeleteEvents(context.Context, []string, Tx) error {
	return nil
}
