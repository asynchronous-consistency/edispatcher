package main

import (
	"bitbucket.org/asynchronous-consistency/edispatcher/v3"
	"bitbucket.org/sveshnikovwork/multiservice/v3"
	"database/sql"
	"fmt"
	log "github.com/sirupsen/logrus"
	prefixed "github.com/x-cray/logrus-prefixed-formatter"
	"time"
)

func main() {
	log.SetLevel(log.InfoLevel)
	log.SetFormatter(new(prefixed.TextFormatter))

	dispatcher := edispatcher.NewEventsDispatcherWithTimeout[*sql.Tx](
		&channel{bus: make(chan string, 500)},
		&store[*sql.Tx]{},
		&receiveStore[*sql.Tx]{},
		&txManager[*sql.Tx]{},
		&busManager{},
		50,
		500,
	)

	isLeader := false
	go func() {
		for {
			isLeader = !isLeader
			log.Print(fmt.Sprintf(`Service change leader status to "%v"`, isLeader))

			time.Sleep(500 * time.Second)
		}
	}()

	isLeaderCallback := func() bool {
		return isLeader
	}

	service := multiservice.MultiService(
		map[string]multiservice.ServiceInterface{
			"Dispatch-Service": edispatcher.NewDispatchService(dispatcher, isLeaderCallback),
			"Receive-Service": edispatcher.NewReceiveService(dispatcher, isLeaderCallback, []edispatcher.EventReceiverInterface[*sql.Tx]{
				&receiver[*sql.Tx]{},
			}),
		},
		39000,
	)

	log.Warning(service.Run())
	log.Print(`All stopped`)
}
