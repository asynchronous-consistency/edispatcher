package edispatcher

import (
	"context"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"sync"
	"time"
)

// Сервис доставки событий в шину. Реализован на основе multiservice
type dispatchService[Tx any] struct {
	isStarted            bool
	isAlive              bool
	ctx                  context.Context
	cancel               context.CancelFunc
	mx                   sync.Mutex
	dispatcher           EventsDispatcherInterface[Tx]
	isServiceCanDispatch func() bool // Callback для определения того, может ли сервис отправлять события
	logger               *logrus.Entry
	timeout              time.Duration
}

// Run запускает сервис
func (d *dispatchService[Tx]) Run() error {
	d.isStarted = true

	d.isAlive = true
	defer func() {
		d.isAlive = false
	}()

	d.mx.Lock()
	defer d.mx.Unlock()

	for {
		select {
		case <-d.ctx.Done():
			return nil
		default:
		}

		if !d.isServiceCanDispatch() {
			time.Sleep(d.timeout)

			continue
		}

		d.logger.Info(`Starting dispatching events to EventBus`)

		rCtx, cancel := context.WithCancel(context.Background())
		g, wCtx := errgroup.WithContext(rCtx)

		g.Go(func() error {
			defer func() {
				cancel()

				d.logger.Info(`Events dispatching complete. Replica is not available to dispatch events to EventBus`)
			}()

			for {
				select {
				case <-d.ctx.Done():
					return nil
				case <-wCtx.Done():
					return nil
				default:
				}

				time.Sleep(d.timeout)
				if !d.isServiceCanDispatch() {
					return nil
				}
			}
		})

		g.Go(func() error {
			time.Sleep(d.timeout)

			return d.dispatcher.SendEventsToBus(wCtx)
		})

		err := g.Wait()
		if nil != err {
			return err
		}
	}
}

// GracefulShutdown выполняет правильную остановку сервиса
func (d *dispatchService[Tx]) GracefulShutdown() {
	d.cancel()

	d.mx.Lock()
	defer d.mx.Unlock()
}

// IsStarted возвращает статус состояния запуска сервиса
func (d *dispatchService[Tx]) IsStarted() bool {
	return d.isStarted
}

// IsAlive возвращает статус сервиса: живой или нет
func (d *dispatchService[Tx]) IsAlive() bool {
	return d.isAlive
}
