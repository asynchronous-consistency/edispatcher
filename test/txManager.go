package main

import "context"

// Менеджер транзакций
type txManager[Tx any] struct {
}

// Begin - Начало транзакции
func (t txManager[Tx]) Begin(context.Context) (Tx, error) {
	var noTx Tx

	return noTx, nil
}

// Rollback - Откат изменений
func (t txManager[Tx]) Rollback(context.Context, Tx) {
}

// Commit - Коммит изменений
func (t txManager[Tx]) Commit(context.Context, Tx) {
}
