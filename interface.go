package edispatcher

import "context"

// EventReceiverInterface описывает подписчика на событие из шины
type EventReceiverInterface[Tx any] interface {
	// Receive выполняет получение и обработку события
	Receive(ctx context.Context, eventData string, tx Tx) error
}

// EventsDispatcherInterface описывает диспетчер событий для реализации
// принципов Event Sourcing
type EventsDispatcherInterface[Tx any] interface {
	// Dispatch выполняет доставку события
	Dispatch(ctx context.Context, eventData string, tx Tx) error

	// Receive выполняет прием и обработку событий
	Receive(ctx context.Context, receivers []EventReceiverInterface[Tx]) error

	// SendEventsToBus выполняет отправку событий в шину
	SendEventsToBus(ctx context.Context) error
}
