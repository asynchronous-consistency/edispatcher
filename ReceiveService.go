package edispatcher

import (
	"context"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"sync"
	"time"
)

// Сервис получения событий из шины. Реализован на основе multiservice
type receiveService[Tx any] struct {
	isStarted           bool
	isAlive             bool
	ctx                 context.Context
	cancel              context.CancelFunc
	mx                  sync.Mutex
	dispatcher          EventsDispatcherInterface[Tx]
	isServiceCanReceive func() bool // Callback для определения того, может ли сервис принимать события
	logger              *logrus.Entry
	receivers           []EventReceiverInterface[Tx]
	timeout             time.Duration
}

// Run - Запуск сервиса
func (r *receiveService[Tx]) Run() error {
	r.isStarted = true

	r.isAlive = true
	defer func() {
		r.isAlive = false
	}()

	r.mx.Lock()
	defer r.mx.Unlock()

	for {
		select {
		case <-r.ctx.Done():
			return nil
		default:
		}

		if !r.isServiceCanReceive() {
			time.Sleep(r.timeout)

			continue
		}

		r.logger.Info(`Starting receiving events from EventBus`)

		rCtx, cancel := context.WithCancel(context.Background())
		g, wCtx := errgroup.WithContext(rCtx)

		g.Go(func() error {
			defer func() {
				cancel()

				r.logger.Info(`Events receiving complete. Replica is not available to receive events from EventBus`)
			}()

			for {
				select {
				case <-r.ctx.Done():
					return nil
				case <-wCtx.Done():
					return nil
				default:
				}

				time.Sleep(r.timeout)
				if !r.isServiceCanReceive() {
					return nil
				}
			}
		})

		g.Go(func() error {
			defer func() {
				cancel()
			}()

			for {
				select {
				case <-wCtx.Done():
					return nil
				default:
				}

				err := r.dispatcher.Receive(wCtx, r.receivers)
				if nil != err {
					return err
				}
			}
		})

		err := g.Wait()
		if nil != err {
			return err
		}
	}
}

// GracefulShutdown - Правильная остановка сервиса
func (r *receiveService[Tx]) GracefulShutdown() {
	r.cancel()

	r.mx.Lock()
	defer r.mx.Unlock()
}

// IsStarted возвращает статус состояния запуска сервиса
func (r *receiveService[Tx]) IsStarted() bool {
	return r.isStarted
}

// IsAlive возвращает статус сервиса: живой или нет
func (r *receiveService[Tx]) IsAlive() bool {
	return r.isAlive
}
