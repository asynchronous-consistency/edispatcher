package edispatcher

import (
	"bitbucket.org/sveshnikovwork/multiservice/v3"
	"context"
	log "github.com/sirupsen/logrus"
	"sync"
	"time"
)

// NewEventsDispatcher реализует фабрику сервиса доставки событий
func NewEventsDispatcher[Tx any](
	channel EventChannelInterface,
	store EventsStoreInterface[Tx],
	receiveStore EventsReceiveStoreInterface[Tx],
	txManager TransactionManagerInterface[Tx],
	busManager BusEventManagerInterface,
	channelPull int,
) EventsDispatcherInterface[Tx] {
	return NewEventsDispatcherWithTimeout[Tx](
		channel,
		store,
		receiveStore,
		txManager,
		busManager,
		channelPull,
		50,
	)
}

// NewEventsDispatcherWithTimeout реализует фабрику сервиса доставки событий
// с настраиваемым таймаутом обработки событий, как доставки так и приема
func NewEventsDispatcherWithTimeout[Tx any](
	channel EventChannelInterface,
	store EventsStoreInterface[Tx],
	receiveStore EventsReceiveStoreInterface[Tx],
	txManager TransactionManagerInterface[Tx],
	busManager BusEventManagerInterface,
	channelPull int,
	timeoutMs uint16,
) EventsDispatcherInterface[Tx] {
	return &eventsDispatcher[Tx]{
		channelPull:  channelPull,
		channel:      channel,
		store:        store,
		receiveStore: receiveStore,
		txManager:    txManager,
		busManager:   busManager,
		logger:       log.WithField("prefix", "edispatcher/EventsDispatcher"),
		timeout:      time.Duration(timeoutMs) * time.Millisecond,
	}
}

// NewDispatchService реализует фабрику сервиса доставки событий в шину
func NewDispatchService[Tx any](
	dispatcher EventsDispatcherInterface[Tx],
	isServiceCanDispatch func() bool,
) multiservice.ServiceInterface {
	return NewDispatchServiceWithTimeout(dispatcher, isServiceCanDispatch, 50)
}

// NewDispatchServiceWithTimeout реализует фабрику сервиса доставки событий в шину
// с настраиваемым таймаутом отправки событий
func NewDispatchServiceWithTimeout[Tx any](
	dispatcher EventsDispatcherInterface[Tx],
	isServiceCanDispatch func() bool,
	timeoutMs uint16,
) multiservice.ServiceInterface {
	ctx, cancel := context.WithCancel(context.Background())

	return &dispatchService[Tx]{
		isStarted:            false,
		isAlive:              false,
		ctx:                  ctx,
		cancel:               cancel,
		mx:                   sync.Mutex{},
		dispatcher:           dispatcher,
		isServiceCanDispatch: isServiceCanDispatch,
		logger:               log.WithField("prefix", "edispatcher/DispatchService"),
		timeout:              time.Duration(timeoutMs) * time.Millisecond,
	}
}

// NewReceiveService реализует фабрику сервиса доставки событий в шину
func NewReceiveService[Tx any](
	dispatcher EventsDispatcherInterface[Tx],
	isServiceCanReceive func() bool,
	receivers []EventReceiverInterface[Tx],
) multiservice.ServiceInterface {
	return NewReceiveServiceWithTimeout[Tx](
		dispatcher,
		isServiceCanReceive,
		receivers,
		50,
	)
}

// NewReceiveServiceWithTimeout реализует фабрику сервиса доставки событий в шину
// с настраиваемым таймаутом приема событий
func NewReceiveServiceWithTimeout[Tx any](
	dispatcher EventsDispatcherInterface[Tx],
	isServiceCanReceive func() bool,
	receivers []EventReceiverInterface[Tx],
	timeoutMs uint16,
) multiservice.ServiceInterface {
	ctx, cancel := context.WithCancel(context.Background())

	return &receiveService[Tx]{
		isStarted:           false,
		isAlive:             false,
		ctx:                 ctx,
		cancel:              cancel,
		mx:                  sync.Mutex{},
		dispatcher:          dispatcher,
		isServiceCanReceive: isServiceCanReceive,
		logger:              log.WithField("prefix", "edispatcher/ReceiveService"),
		receivers:           receivers,
		timeout:             time.Duration(timeoutMs) * time.Millisecond,
	}
}
