package main

import (
	"context"
)

// Store для меток полученных событий
type receiveStore[Tx any] struct {
	iter int
}

// GetLastReceivedEventId - Получение ID последнего принятого события
func (r *receiveStore[Tx]) GetLastReceivedEventId(context.Context, Tx) (string, error) {
	return "0", nil
}

// SetLastReceivedEventId - Установка ID последнего принятого события
func (r *receiveStore[Tx]) SetLastReceivedEventId(context.Context, string, Tx) error {
	r.iter++

	//if r.iter > 100 {
	//	return errors.New(`test`)
	//}

	return nil
}
