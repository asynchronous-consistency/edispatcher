package edispatcher

import (
	"bitbucket.org/go-generics/h"
	"context"
	"encoding/json"
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"golang.org/x/sync/errgroup"
	"time"
)

const ShortWaitTimeout = 2 * time.Second
const LongWaitTimeout = 30 * time.Second

// eventsDispatcher реализует диспетчер событий для реализации
// принципов Event Sourcing
type eventsDispatcher[Tx any] struct {
	channelPull  int                             // Размер пула для очереди обработки событий
	channel      EventChannelInterface           // Канал для доставки событий
	store        EventsStoreInterface[Tx]        // Store для временного хранения событий
	receiveStore EventsReceiveStoreInterface[Tx] // Store для хранения меток принятых событий
	txManager    TransactionManagerInterface[Tx] // Менеджер транзакций
	busManager   BusEventManagerInterface        // Менеджер событий на стороне шины
	logger       *logrus.Entry
	timeout      time.Duration
}

// Dispatch выполняет доставку события
func (e eventsDispatcher[Tx]) Dispatch(ctx context.Context, eventData string, tx Tx) error {
	e.logger.WithFields(logrus.Fields{
		"code": 100,
		"data": eventData,
	}).Debug(`Store event to temporary storage`)

	return e.store.CreateEvents(ctx, []string{eventData}, tx)
}

// Receive выполняет прием и обработку событий
func (e eventsDispatcher[Tx]) Receive(ctx context.Context, receivers []EventReceiverInterface[Tx]) error {
	var noTx Tx

	ctx = h.ValueOrDefaultFn[context.Context](ctx, context.Background(), h.IsNil[context.Context])
	if h.IsCtxDone(ctx) {
		return nil
	}

	eventsChan := make(chan *EventTemporaryData, e.channelPull)
	defer close(eventsChan)

	// Рабочий контекст. Служит для остановки рабочих групп
	gCtx, cancel := context.WithCancel(ctx)
	g, wCtx := errgroup.WithContext(gCtx)

	// Запускаем чтение событий из канала
	g.Go(func() error {
		lastEvent, err := h.WorSTwo(e.receiveStore.GetLastReceivedEventId)(LongWaitTimeout)(wCtx, noTx)
		if nil != err {
			return errors.Wrap(err, `failed to get last received ID`)
		}

		return e.channel.Receive(wCtx, lastEvent, eventsChan)
	})

	// Запускаем прием событий
	g.Go(func() (err error) {
		defer func() {
			cancel()

			justSkip := false
			for 0 != len(eventsChan) {
				event := <-eventsChan

				if justSkip {
					continue
				}

				// Если не отвечает шина на отказ от приема, значит нечего туда
				// больше посылать что-то, она повисла.
				err := h.EorSTwo(e.busManager.RejectEvent)(ShortWaitTimeout)(ctx, event)
				if nil != err {
					justSkip = true
				}
			}
		}()

		for !h.IsCtxDone(wCtx) {
			var event *EventTemporaryData
			select {
			case event = <-eventsChan:
				break
			default:
				time.Sleep(e.timeout)
				continue
			}

			tx, err := h.WorSOne(e.txManager.Begin)(LongWaitTimeout)(wCtx)
			if nil != err {
				return errors.Wrap(err, `failed to start transaction`)
			}

			rollback := func(err error, wrap string) error {
				h.NorSTwo(e.txManager.Rollback)(ShortWaitTimeout)(ctx, tx)
				_ = h.EorSTwo(e.busManager.RejectEvent)(ShortWaitTimeout)(ctx, event)

				return errors.Wrap(err, wrap)
			}

			for _, receiver := range receivers {
				err := receiver.Receive(wCtx, event.Event.Data, tx)
				if nil != err {
					return rollback(err, `failed to receive events`)
				}
			}

			err = h.EorSThree(e.receiveStore.SetLastReceivedEventId)(LongWaitTimeout)(wCtx, event.Event.Id, tx)
			if nil != err {
				return rollback(err, `failed to set last received ID`)
			}

			// Коммит в шине ждем подольше, т.к. он происходит уже по факту выполнения
			// задания и очень не хочется все откатывать. Но все же, если шина не ответит,
			// то откатываем все, что сделано.
			err = h.EorSTwo(e.busManager.CommitEvent)(LongWaitTimeout)(wCtx, event)
			if nil != err {
				return rollback(err, `failed to commit event in EventBus`)
			}

			h.NorSTwo(e.txManager.Commit)(LongWaitTimeout)(wCtx, tx)

			e.logger.WithFields(logrus.Fields{
				"code":   200,
				"offset": event.Event.Id,
				"event":  event.Event.Data,
			}).Info(`Received event from EventBus`)
		}

		return nil
	})

	err := g.Wait()
	if nil != err {
		e.logger.WithError(err).WithField("code", 500).Error("Failed to receive events")
	}

	return err
}

// SendEventsToBus выполняет отправку событий в шину
func (e eventsDispatcher[Tx]) SendEventsToBus(ctx context.Context) error {
	if nil == ctx {
		ctx = context.Background()
	}

	for !h.IsCtxDone(ctx) {
		tx, err := h.WorSOne(e.txManager.Begin)(LongWaitTimeout)(ctx)
		if nil != err {
			return errors.Wrap(err, `failed to start transaction`)
		}

		events, err := h.WorSTwo(e.store.LoadEvents)(LongWaitTimeout)(ctx, tx)
		if nil != err {
			h.NorSTwo(e.txManager.Rollback)(ShortWaitTimeout)(ctx, tx)

			e.logger.
				WithError(err).
				WithField("code", 500).
				Error(`Failed to load events from temporary store`)

			return errors.Wrap(err, `failed to load events from temporary store`)
		}

		if 0 == len(events) {
			h.NorSTwo(e.txManager.Commit)(ShortWaitTimeout)(ctx, tx)

			// Задержка нужна для того, чтоб не дудосить свою же БД
			time.Sleep(e.timeout)

			continue
		}

		eventIds := make([]string, len(events))
		eventsData := make([]string, len(events))
		for i, event := range events {
			eventsData[i] = event.Data
			eventIds[i] = event.Id
		}

		err = h.EorSTwo(e.channel.Dispatch)(LongWaitTimeout)(ctx, eventsData)
		if nil != err {
			h.NorSTwo(e.txManager.Rollback)(ShortWaitTimeout)(ctx, tx)

			e.logger.
				WithError(err).
				WithField("code", 500).
				Error(`Failed to send events to EventBus`)

			return errors.Wrap(err, `failed to send events to event bus`)
		}

		err = h.EorSThree(e.store.DeleteEvents)(LongWaitTimeout)(ctx, eventIds, tx)
		if nil != err {
			h.NorSTwo(e.txManager.Rollback)(ShortWaitTimeout)(ctx, tx)

			e.logger.
				WithError(err).
				WithField("code", 500).
				Error(`Failed to delete temporary events`)

			return errors.Wrap(err, `failed to delete temporary events`)
		}

		h.NorSTwo(e.txManager.Commit)(LongWaitTimeout)(ctx, tx)

		eventsDataBytes, _ := json.Marshal(events)
		e.logger.WithFields(logrus.Fields{
			"code":   200,
			"events": string(eventsDataBytes),
		}).Debug(`Events is dispatched to EventBus`)
	}

	return nil
}
